var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
    return gulp.src('sass/main.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['node_modules/foundation-sites/scss', './sass', 'sass/_*.scss']
        }))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
        }))
        .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'))
});

gulp.task("watch", function(){
    gulp.watch("sass/*.scss", ["sass"])
});

gulp.task('default', ['sass', 'watch']);



